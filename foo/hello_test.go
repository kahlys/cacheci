package foo

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestFoo(t *testing.T) {
	time.Sleep(10 * time.Second)
	actual, err := Hello("foo")
	require.NoError(t, err)
	assert.Equal(t, "Hello, foo!", actual)
}
