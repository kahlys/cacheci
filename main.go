package main

import (
	"flag"
	"fmt"

	"cacheci/foo"
)

func main() {
	var name = flag.String("name", "foo", "name to say hello")
	flag.Parse()

	actual, err := foo.Hello(*name)
	if err != nil {
		panic(err)
	}
	fmt.Println(actual)
}
